package chapter10.DisplayingFrames2.FrameProperties2;

import javax.swing.*;
import java.awt.*;

public class SimpleFrameTest {
    public static void main(String[] args) {
        EventQueue.invokeLater(
                () -> {
                    var frame = new SimpleFrame();
                    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    frame.setVisible(true);
                }
        );

        System.out.println("that's all");
    }

    private static class SimpleFrame extends JFrame {
        private static final int DEFAULT_WIDTH = Toolkit.getDefaultToolkit().getScreenSize().width/2;
        private static final int DEFAULT_HEIGHT = Toolkit.getDefaultToolkit().getScreenSize().height/2;
        public SimpleFrame() {
            setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
            setResizable(true);
        }
    }
}
