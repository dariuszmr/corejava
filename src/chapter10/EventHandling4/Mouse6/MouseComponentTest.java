package chapter10.EventHandling4.Mouse6;

import javax.swing.*;
import java.awt.*;

public class MouseComponentTest {
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            JFrame jFrame = new JFrame();
            jFrame.setSize(300, 200);
            jFrame.add(new MouseComponent());
            jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            jFrame.setVisible(true);
        });
    }
}
