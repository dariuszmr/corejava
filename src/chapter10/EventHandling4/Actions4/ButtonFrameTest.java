package chapter10.EventHandling4.Actions4;

import javax.swing.*;
import java.awt.*;

public class ButtonFrameTest {
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            JFrame buttonFrame = new ButtonFrame();
            buttonFrame.setVisible(true);
            buttonFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        });
    }
}
