package chapter10.EventHandling4.Actions4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class ButtonFrame extends JFrame {
    private final JPanel buttonPanel;
    private static final int DEFAULT_WITDH = 300;
    private static final int DEFAULT_HEIGHT = 400;

    public ButtonFrame() {
        setSize(DEFAULT_WITDH , DEFAULT_HEIGHT);
        buttonPanel = new JPanel();
        add(buttonPanel);

        InputMap inputMap = buttonPanel.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke("ctrl B"), "buttonPanel.blue");
        ActionMap actionMap = buttonPanel.getActionMap();


        makeButton("Red", Color.red);
        makeButton("Green", Color.green);

        var blueAction = new ColorAction("Blue", Color.blue);
        var blueButton = new JButton(blueAction);
        buttonPanel.add(blueButton);

        actionMap.put("buttonPanel.blue", blueAction);

    }

    private void makeButton(String name, Color backgroundColor) {
        var button = new JButton(name);
        button.addActionListener(e -> {
            buttonPanel.setBackground(backgroundColor);
        });
        buttonPanel.add(button);

    }

    class ColorAction extends AbstractAction {

        public ColorAction(String name, Color color) {
            super(name);
            putValue("color", color);
            putValue(Action.NAME, name);
            putValue(Action.SHORT_DESCRIPTION, "it's short description");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            Color color = (Color) getValue("color");
            buttonPanel.setBackground(color);
        }
    }

}

