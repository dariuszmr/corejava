package chapter10.EventHandling4.Example2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonFrame extends JFrame {
    private JPanel buttonPanel;
    private static final int DEFAULT_WITDH = 300;
    private static final int DEFAULT_HEIGHT = 200;

    public ButtonFrame() {
        setSize(DEFAULT_WITDH, DEFAULT_HEIGHT);

//        var yellowButton = new JButton("Yellow");
//        var blueButton = new JButton("Blue");
//        var redButton = new JButton("Red");

        buttonPanel = new JPanel();

//        buttonPanel.add(yellowButton);
//        buttonPanel.add(blueButton);
//        buttonPanel.add(redButton);

        add(buttonPanel);

//        var yellowAction = new ColorAction(Color.YELLOW);
//        var blueAction = new ColorAction(Color.BLUE);
//        var redAction = new ColorAction(Color.RED);

//        yellowButton.addActionListener(yellowAction);
//        blueButton.addActionListener(blueAction);
//        redButton.addActionListener(redAction);

        makeButton("Red", Color.red);
        makeButton("Green", Color.green);
        makeButton("Yellow", Color.yellow);
    }

    private class ColorAction implements ActionListener {

        private Color background;
        public ColorAction(Color c) {
            background = c;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            buttonPanel.setBackground(background);
        }
    }

    public void makeButton(String name, Color colorBackground) {
        var button = new JButton(name);
        buttonPanel.add(button);
        button.addActionListener(e -> {
            buttonPanel.setBackground(colorBackground);
        });
    }
}
