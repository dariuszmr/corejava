package chapter10.EventHandling4.Example2;

import java.awt.EventQueue;
import javax.swing.JFrame;

public class ButtonFrameTest {
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            ButtonFrame buttonFrame = new ButtonFrame();
            buttonFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            buttonFrame.setVisible(true);
        });
    }
}
