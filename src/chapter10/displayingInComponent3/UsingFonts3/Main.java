package chapter10.displayingInComponent3.UsingFonts3;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(GraphicsEnvironment
                .getLocalGraphicsEnvironment()
                .getAvailableFontFamilyNames()));


        EventQueue.invokeLater(() -> {
            JFrame jFrame = new JFrame();
            jFrame.setSize(400, 400);
            jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            jFrame.setVisible(true);


            MyComponent myComponent = new MyComponent();
            jFrame.add(myComponent);


        });
    }
}

class MyComponent extends JComponent {
    public void paintComponent(Graphics g) {
        Graphics2D graphics2D = (Graphics2D) g;
        g.drawString("siema", 100, 100);

        var snasbold15 = new Font("SansSerif", Font.BOLD, 14);
        g.setFont(snasbold15);
        g.drawString("Hejo", 200, 200);
    }
}
