package chapter10.displayingInComponent3.Shapes2D1;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

public class DrawTest {
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            var frame = new DrawFrame();
            frame.setTitle("DrawTest");
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setVisible(true);
        });
    }
}

class DrawFrame extends JFrame {
    public DrawFrame() {
        DrawComponent drawComponent = new DrawComponent();
        drawComponent.setBackground(Color.blue);
        drawComponent.setForeground(Color.pink);
        add(drawComponent);
        pack();
    }
}

class DrawComponent extends JComponent {
    private static final int DEFAULT_WIDTH = 400;
    private static final int DEFAULT_HEIGHT = 400;

    public void paintComponent(Graphics g) {
        var g2 = (Graphics2D) g;
        int leftX = 100;
        int topY = 100;
        int width = 200;
        int height = 150;
        var rect = new Rectangle2D.Double(leftX, topY, width, height);
        g2.draw(rect);

        var ellipse = new Ellipse2D.Double();
        ellipse.setFrame(rect);
        g2.draw(ellipse);

        g2.draw(new Line2D.Double(leftX, topY, leftX + width, topY + height));

        double centerX = rect.getCenterX();
        double centerY = rect.getCenterY();
        double radius = 150;
        var circle = new Ellipse2D.Double();
        circle.setFrameFromCenter(centerX, centerY, centerX + radius, centerY + radius);
        g2.draw(circle);

        g2.setColor(Color.blue);
//        g2.fill(ellipse);
        g2.drawString("Hejo", 100,100);

        g2.setColor(new Color(0, 128, 255));
        g2.drawString("Kolorki", 200, 200);
//        g2.setBackground(Color.CYAN);
    }

    public Dimension getPreferredSize() {
        return new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }
}
