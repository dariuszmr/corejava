package chapter6.lambda2;

import java.awt.event.ActionListener;
import java.time.Instant;
import java.util.Comparator;

public class Main {

    public static void main(String[] args) {
        Runnable runnable = () -> {
            for (int i = 0; i < 100; i++) {
                System.out.println(i);
            }
        };
        Comparator<String> comparator = (first, second) -> first.length() - second.length();

        ActionListener listener = event -> System.out.println("The time is " + Instant.ofEpochMilli(event.getWhen()));

    }

}
