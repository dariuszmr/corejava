package chapter6.functionalInterfaces3;

import java.awt.Toolkit;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import javax.swing.Timer;

public class FunctionalInterfaces {

    public static void main(String[] args) {
        var timer = new Timer(1000, event -> {
            System.out.println("At the tone, the time is " + Instant.ofEpochMilli(event.getWhen()));
            Toolkit.getDefaultToolkit().beep();
        });

        BiFunction<String, String, Integer> comp = (first, second) -> first.length() - second.length();
        var planets = new String [] { "Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune" };
        //Arrays.sort(planets, (first, second) -> first.length() - second.length());
        System.out.println(Arrays.toString(planets));
        //
        ArrayList<String> planetsArray = new ArrayList<>();
        planetsArray.addAll(List.of(planets));

        System.out.println(planetsArray);

        Predicate<String> predicate = new Predicate<String>() {
            @Override
            public boolean test(String s) {
                return s.startsWith("M");
            }
        };
        //planetsArray.removeIf(e -> e.startsWith("M"));
        planetsArray.removeIf(predicate);
        System.out.println(planetsArray);

        LocalDate day =  LocalDate.of(2000, 10, 10);
        //LocalDate hireDay = Objects.requireNonNullElse(day, LocalDate.of(1978, 12, 28));
        LocalDate hireDay = Objects.requireNonNullElseGet(day,() -> LocalDate.of(1978, 12, 28));

        System.out.println(hireDay);
    }

}
