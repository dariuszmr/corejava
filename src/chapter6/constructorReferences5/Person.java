package chapter6.constructorReferences5;

public class Person {
    private String name;

    public Person() {
        this.name = "default";
    }

    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
