package chapter6.constructorReferences5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        ArrayList<String> names = new ArrayList<>(List.of("darek", "marek"));
        System.out.println(names);
        Stream<Person> stream = names.stream().map(Person::new);
//        List<Person> personList = stream.toList();
//        for (Person person :
//                personList) {
//            System.out.println(person.getName());
//        }

        Person[] persons = stream.toArray(Person[]::new);
        for (Person person :
                persons) {
            System.out.println(person.getName());
        }
    }
}
