package chapter6.moreAboutComparators8;

import java.util.Arrays;
import java.util.Comparator;

public class Main {
    public static void main(String[] args) {
        Person[] people = new Person[3];
        people[0] = new Person("józekkkkkkkkkk", "mrugala");
        people[1] = new Person("darek", "mrugala");
        people[2] = new Person("monia", "rataj");
//        = {new Person("darek", "mrugala"), new Person("monia", "rataj")};

        System.out.println(Arrays.toString(people));

        Arrays.sort(people, Comparator.comparing(Person::getLastName).thenComparing(Person::getFirstName));
        System.out.println(Arrays.toString(people));

//        Arrays.sort(people, Comparator.comparing(Person::getFirstName, (s,t) -> Integer.compare(s.length(), t.length())));
//        System.out.println(Arrays.toString(people));

        Arrays.sort(people, Comparator.comparingInt(p -> p.getFirstName().length()));
        System.out.println(Arrays.toString(people));
    }
}
