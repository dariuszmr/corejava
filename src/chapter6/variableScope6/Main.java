package chapter6.variableScope6;

import java.awt.Toolkit;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import javax.swing.Timer;

public class Main {

    public static void main(String[] args) {
        repeatMessage("hejo", 1000);
    }

    public static void repeatMessage(String text, int delay) {
        ActionListener listener = event -> {
            System.out.println(text);
            Toolkit.getDefaultToolkit().beep();
        };

        new Timer(delay, listener).start();

        JOptionPane.showMessageDialog(null, "end?");
        System.exit(0);
    }

    //public static void countDown(int start, int delay) {
    //    ActionListener listener = (event) -> {
    //        start--;
    //        System.out.println(start);
    //    };
    //}

    //public static void repeat(String text, int count) {
    //    for (int i = 0; i < 10; i++) {
    //        ActionListener listener = (event) -> {
    //            System.out.println(i);
    //        };
    //    }
    //}


}
