package chapter6.interfaces1.interfaceconcept1;

import chapter5.Employee;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        var classImplementedComparableInterface = new ClassImplementedComparableInterface();
        if (classImplementedComparableInterface.compareTo(new ClassImplementedComparableInterface()) > 0) {
            System.out.println("coś jest większe od czegoś");
        }

        final int sizeOfArray = 4;
        Employee[] employees = new Employee[sizeOfArray];
        for (int i = 0; i < employees.length; i++) {
            employees[i] = new Employee("darek" + i, (sizeOfArray - i) * 10_000, 2023, 5, 10 + i);
        }

        System.out.println(Arrays.toString(employees));

        Arrays.sort(employees);

        System.out.println(Arrays.toString(employees));
    }
}
