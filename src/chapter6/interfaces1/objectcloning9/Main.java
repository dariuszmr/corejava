package chapter6.interfaces1.objectcloning9;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) throws CloneNotSupportedException {
        //Employee employee = new Employee("Darek", 10);
        //Employee copyEmployee = employee.clone();

        var someClonableClass = new SomeClonableClass("clonable");
        SomeClonableClass copy = someClonableClass.clone();

        System.out.println(someClonableClass);
        System.out.println(copy);

        int[] arrayInt = {1,2,3,4,5};
        int[] copyArrayInt = arrayInt.clone();
        copyArrayInt[0] = 1000;
        System.out.println(Arrays.toString(arrayInt));
        System.out.println(Arrays.toString(copyArrayInt));
    }

}
