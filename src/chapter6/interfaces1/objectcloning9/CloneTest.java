package chapter6.interfaces1.objectcloning9;

public class CloneTest {

    public static void main(String[] args) throws CloneNotSupportedException {
        var original = new Employee("John", 50_000);
        original.setHireDay(2000,1,1);
        Employee copy = original.clone();
        copy.raiseSalary(10);
        copy.setHireDay(2023,5,29);
        System.out.println("original " + original);
        System.out.println("copy " + copy);
    }

}
