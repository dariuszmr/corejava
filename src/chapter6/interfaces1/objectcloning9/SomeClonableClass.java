package chapter6.interfaces1.objectcloning9;

public class SomeClonableClass implements Cloneable{
    private final String name;

    public SomeClonableClass(String name) {
        this.name = name;
    }

    @Override
    public SomeClonableClass clone() throws CloneNotSupportedException {
        return (SomeClonableClass) super.clone();
    }

    @Override
    public String toString() {
        return "SomeClonableClass{" +
            "name='" + name + '\'' +
            '}';
    }
}
