package chapter6.interfaces1.callbacks7;

import javax.swing.*;

public class TimerTest {
    public static void main(String[] args) {
        var listener = new TimerPrinter();

        var timer = new Timer(1000, listener);
        timer.start();

        JOptionPane.showMessageDialog(null, "Quit program?");
        System.exit(0);
    }
}
