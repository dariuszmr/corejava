package chapter6.interfaces1.callbacks7;

import java.awt.*;
import java.time.Instant;

public class Main {
    public static void main(String[] args) {
        Toolkit.getDefaultToolkit().beep();
        System.out.println(Instant.now());
    }
}
