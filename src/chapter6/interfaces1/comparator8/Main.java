package chapter6.interfaces1.comparator8;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        String[] friends = {"Darek", "Marta", "Jan", "Telesfor"};
        System.out.println(Arrays.toString(friends));
        Arrays.sort(friends);
        System.out.println(Arrays.toString(friends));
        Arrays.sort(friends, new LengthComparator());
        System.out.println(Arrays.toString(friends));
    }
}
