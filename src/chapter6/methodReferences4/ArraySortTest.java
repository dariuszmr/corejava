package chapter6.methodReferences4;

import java.util.Arrays;
import java.util.Objects;

import static java.io.File.separator;

public class ArraySortTest {
    public static void main(String[] args) {
        var planets = new String [] { "Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune" };
        Arrays.sort(planets, String::compareToIgnoreCase);
        System.out.println(Arrays.toString(planets));

        Objects.isNull(planets);

//        separator::equals

    }
}
