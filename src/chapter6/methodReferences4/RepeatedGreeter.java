package chapter6.methodReferences4;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class RepeatedGreeter extends Greeter{
    public void greet(ActionEvent event) {
        var timer = new Timer(1000, super::greet);
        timer.start();
    }
}
