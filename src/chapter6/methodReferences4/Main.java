package chapter6.methodReferences4;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Main {
    public static void main(String[] args) {
        ActionListener actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("hejo");
            }
        };

//        Runnable runnable = System.out.println();

        //        var timer = new Timer(1000, e -> System.out.println(e));
        var timer = new Timer(1000, System.out::println);
        timer.start();

        JOptionPane.showMessageDialog(null,"end");
        System.exit(0);
    }
}
