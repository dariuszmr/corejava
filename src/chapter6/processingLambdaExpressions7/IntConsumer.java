package chapter6.processingLambdaExpressions7;

public interface IntConsumer {
    void accept(int value);
}
