package chapter6.processingLambdaExpressions7;

import java.util.function.Consumer;

public class IntConsumerTest {
    public static void main(String[] args) {
        repeat(10, (i) -> System.out.println(i));
        System.out.println("--------");
        repeatWithConsumer(10, (i) -> System.out.println(i));
    }

    public static void repeat(int n, IntConsumer action) {
        for (int i = 0; i < n; i++) {
            action.accept(i);
        }
    }

    public static void repeatWithConsumer(int n, Consumer<Integer> consumer){
        for (int i = 0; i < n; i++) {
            consumer.accept(i);
        }
    }


}
