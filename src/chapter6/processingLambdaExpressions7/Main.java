package chapter6.processingLambdaExpressions7;

public class Main {

    public static void main(String[] args) {
        repeat(10, () -> System.out.println("hej"));
    }

    public static void repeat(int n, Runnable action) {
        for (int i = 0; i < n; i++) {
            action.run();
        }
    }

}
