package chapter6.InnerClasses3.SpecialSyntaxRules2;

import javax.swing.JOptionPane;

public class InnerClassTest {

    public static void main(String[] args) {
        var clock = new TalkingClock(1000, true);

        //try to create listener which is in TalkingClock as inner class
        TalkingClock.TimePrinter listener = clock.new TimePrinter();

        clock.start();

        JOptionPane.showMessageDialog(null, "end?");
        System.exit(0);
    }

}
