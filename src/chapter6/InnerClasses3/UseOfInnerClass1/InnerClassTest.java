package chapter6.InnerClasses3.UseOfInnerClass1;

import javax.swing.JOptionPane;

public class InnerClassTest {

    public static void main(String[] args) {
        var clock = new TalkingClock(1000, true);
        clock.start();

        JOptionPane.showMessageDialog(null, "end?");
        System.exit(0);
    }

}
