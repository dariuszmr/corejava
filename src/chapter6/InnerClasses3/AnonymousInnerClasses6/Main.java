package chapter6.InnerClasses3.AnonymousInnerClasses6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        System.out.println( new ArrayList<String>() {{add("darek"); add("marek");}}  );

        String strings[] = {"darek", "marek"};
        System.out.println(Arrays.toString(strings));

        System.out.println(Arrays.toString(new String[]{"darek", "marek"}));

        printList( new ArrayList<>() {{add("abc"); add("qwe");}} );

        printList(List.of("ere", "zcz"));

        TestGetClass testGetClass = new TestGetClass();
        testGetClass.printGetClass();

        System.out.println("from main getClass " + new Object(){}.getClass().getEnclosingClass());
    }

    public static void printList(List<String> listToPrint) {
        System.out.println("try to getClass from static " + new Object(){}.getClass().getEnclosingClass());
        System.out.println(listToPrint);
    }
}
