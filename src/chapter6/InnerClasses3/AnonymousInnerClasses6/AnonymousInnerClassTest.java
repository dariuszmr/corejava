package chapter6.InnerClasses3.AnonymousInnerClasses6;

import javax.swing.*;

public class AnonymousInnerClassTest {
    public static void main(String[] args) {
        var clock = new TalkingClock();
        clock.start(1000, true);

        JOptionPane.showMessageDialog(null, "end?");
        System.exit(0);
    }
}
