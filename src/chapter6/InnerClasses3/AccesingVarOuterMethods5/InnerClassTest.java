package chapter6.InnerClasses3.AccesingVarOuterMethods5;

import javax.swing.*;

public class InnerClassTest {

    public static void main(String[] args) {
        var clock = new TalkingClock();

        clock.start(1000, true);

        JOptionPane.showMessageDialog(null, "end?");
        System.exit(0);
    }

}
