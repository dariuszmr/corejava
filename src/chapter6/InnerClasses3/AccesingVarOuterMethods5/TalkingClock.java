package chapter6.InnerClasses3.AccesingVarOuterMethods5;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.Instant;

public class TalkingClock {

    public void start(int interval, boolean beep) {
        class TimePrinter implements ActionListener {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("At the tone, the time is " + Instant.ofEpochMilli(e.getWhen()));
                if (beep) {
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        }

        var listener = new TimePrinter();
        var timer = new Timer(interval, listener);
        timer.start();
    }

}
