package chapter6.InnerClasses3.StaticInnerClasses7;

public class Main {
    public static void main(String[] args) {
        double max = Double.MAX_VALUE;
        System.out.println(max);
        double positivMax = Double.POSITIVE_INFINITY;
        System.out.println(positivMax);
        if (Double.MAX_VALUE < Double.POSITIVE_INFINITY) {
            System.out.println("Double.MAX_VALUE < Double.POSITIVE_INFINITY");
        }
        if (Double.MAX_VALUE > Double.POSITIVE_INFINITY) {
            System.out.println("Double.MAX_VALUE > Double.POSITIVE_INFINITY");
        }
    }
}
