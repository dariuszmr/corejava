package chapter8.Restrictions6.RawTypes2;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class Main {

    public static void main(String[] args) {
        //int a = 0;
        //Integer a = 0;
        //Pair<String> a = new Pair<>("a", "b");
        Pair<Integer> a = new Pair<>(Integer.MIN_VALUE, Integer.MAX_VALUE);

        //if (a instanceof Pair<String>) {
        //    System.out.println("a is a pair of strings");
        //}

        //if (a instanceof Pair<T>) {
        //    System.out.println("some type");
        //}

        //Pair<String> p = (Pair<String>) a;


        Pair<Double> b = new Pair<>(Double.MIN_VALUE, Double.MAX_VALUE);

        if (a.getClass() == b.getClass()) {
            System.out.println(a.getClass());
            System.out.println(b.getClass());
            Field[] declaredFields = a.getClass().getDeclaredFields();
            for (Field f :
                declaredFields) {
                System.out.println(Modifier.toString(f.getModifiers()) + " " + f.getType() + " " + f.getName());
            }
            System.out.println(a.getClass().getDeclaredFields());
            System.out.println("a = b ????");
        }
    }

}
