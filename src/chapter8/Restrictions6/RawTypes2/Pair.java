package chapter8.Restrictions6.RawTypes2;

public class Pair<T> {
    private T first;
    private T second;


    public Pair(T min, T max) {
        this.first = min;
        this.second = max;
    }

    public T getFirst() {
        return first;
    }

    public T getSecond() {
        return second;
    }
}
