package chapter8.Restrictions6.CannotInstantiateType5;

import java.util.function.Supplier;

public class Pair<T> {
    private T first;
    private T second;

    //public Pair() {
    //    first = new T();
    //    second = new T();
    //}


    public Pair(T min, T max) {
        this.first = min;
        this.second = max;
    }

    public static <T> Pair<T> makePair(Supplier<T> constructor) {
        return new Pair<>(constructor.get(), constructor.get());
    }

    public T getFirst() {
        return first;
    }

    public T getSecond() {
        return second;
    }
}
