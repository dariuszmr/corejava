package chapter8.Restrictions6.CannotInstantiateType5;

public class Main {

    public static void main(String[] args) {
        Pair<String> p = Pair.makePair(String::new);

        System.out.println(p.getFirst());
    }

}
