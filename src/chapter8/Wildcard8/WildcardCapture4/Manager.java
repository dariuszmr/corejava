package chapter8.Wildcard8.WildcardCapture4;

public class Manager extends Employee{

    private Double bonus;

    public Manager(String name, Double salary, int year, int month, int day) {
        super(name, salary, year, month, day);
    }

    public void setBonus(Double bonus) {
        this.bonus = bonus;
    }

    public Double getBonus() {
        return this.bonus;
    }
}
