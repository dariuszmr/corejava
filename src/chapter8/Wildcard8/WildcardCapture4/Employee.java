package chapter8.Wildcard8.WildcardCapture4;

import java.time.LocalDate;
import java.util.Date;

public class Employee {
    private String name;
    private Double salary;
    private Date hireDay;

    public Employee() {
        this.name = "default";
        this.salary = 0.0;
        this.hireDay = new Date();
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", salary=" + salary +
                ", hireDay=" + hireDay +
                '}';
    }

    public Employee(String name) {
        this(name, 0.0, LocalDate.now().getYear(), LocalDate.now().getMonthValue(), LocalDate.now().getDayOfMonth());
    }

    public Employee(String name, Double salary, int year, int month, int day) {
        this.name = name;
        this.salary = salary;
        this.hireDay = new Date(year, month, day);
    }

    public String getName() {
        return name;
    }

    public Double getSalary() {
        return salary;
    }

    public Date getHireDay() {
        return hireDay;
    }
}
