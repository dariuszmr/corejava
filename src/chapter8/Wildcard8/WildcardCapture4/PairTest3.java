package chapter8.Wildcard8.WildcardCapture4;

public class PairTest3 {

    public static void main(String[] args) {
        var ceo = new Manager("Gus Greedy", 800000.0, 2003, 12, 15);
        var cfo = new Manager("Sid Sneaky", 600000.0, 2003, 12, 15);

        var buddies = new Pair<Manager>(ceo, cfo);
        printBuddies(buddies);

        ceo.setBonus(1000000.0);
        cfo.setBonus(50000.0);
        Manager[] managers = {ceo, cfo};

        var result = new Pair<Employee>();
        minmaxBonus(managers, result);

        System.out.println("first: " + result.getFirst().getName() + ", second: " + result.getSecond().getName());

        maxminBonus(managers, result);

        System.out.println("first: " + result.getFirst().getName() + ", second: " + result.getSecond().getName());

    }

    private static void maxminBonus(Manager[] managers, Pair<? super Manager> result) {
        minmaxBonus(managers, result);
        PairAlg.swapHelper(result);
    }

    private static void minmaxBonus(Manager[] managers, Pair<? super Manager> result) {
        if (managers.length == 0) return;
        Manager min = managers[0];
        Manager max = managers[0];
        for (int i = 0; i < managers.length; i++) {
            if (min.getBonus() > managers[i].getBonus()) min = managers[i];
            if (max.getBonus() < managers[i].getBonus()) max = managers[i];
        }
        result.setFirst(min);
        result.setSecond(max);
    }

    private static void printBuddies(Pair<? extends Employee> buddies) {
        System.out.println(buddies.getFirst().getName() + " and " + buddies.getSecond().getName() + " are buddies.");
    }

    private static class PairAlg {
        public static <T> void swapHelper(Pair<T> p) {
            T t = p.getFirst();
            p.setFirst(p.getSecond());
            p.setSecond(t);
        }
    }
}
