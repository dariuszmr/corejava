package chapter8.ReflectionGenerics9.TypeMatching2;

import chapter8.Wildcard8.WildcardCapture4.Employee;
import chapter8.Wildcard8.WildcardCapture4.Pair;

public class Main {

    public static void main(String[] args) {
        try {
            Pair<Employee> employeePair = makePair(Employee.class);
//            System.out.println(employeePair);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> Pair<T> makePair(Class<T> c) throws InstantiationException, IllegalAccessException {
        return new Pair<>(c.newInstance(), c.newInstance());
    }
}

