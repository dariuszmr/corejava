package chapter8.ReflectionGenerics9.TypeMatching2;

import chapter6.constructorReferences5.Person;

public class NewInstanceTest {
    public static void main(String[] args) {
        try {
            Person darek = Person.class.newInstance();
            System.out.println(darek);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }

    }
}
