package chapter8.GenericMethods3;

public class ArrayAlg {
    public static <T> T getMiddle(T... a) {
        return a[a.length/2];
    }
}
