package chapter9.Maps4.UpdatingEntries2;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Map<String, Integer> counts = new HashMap<>();

        counts.put("word1", 3);
        counts.put("word2", 23);
        counts.put("word3", 2);

        System.out.println(counts);

        counts.put("word1", counts.get("word1") + 1);
        System.out.println(counts);

        counts.merge("word4", 1, Integer::sum);
        System.out.println(counts);

        String word5 = "word5";
        counts.put(word5, counts.getOrDefault(word5, 0) + 1);
        System.out.println(counts);

        counts.forEach((k,v) -> {
            System.out.println("key:" + k + ", value:" +v);
        });

        for (Map.Entry<String, Integer> entry : counts.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }

        Set<String> keys = counts.keySet();
        keys.remove("word1");
        System.out.println(counts);

        Collection<Integer> values = counts.values();
        values.remove(23);
        System.out.println(counts);

//        List.of()
    }
}
