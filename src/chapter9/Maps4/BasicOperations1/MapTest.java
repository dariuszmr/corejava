package chapter9.Maps4.BasicOperations1;

import chapter8.Wildcard8.WildcardCapture4.Employee;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.TreeMap;

public class MapTest {
    public static void main(String[] args) {
        var staff = new HashMap<String, Employee>();
        staff.put("144-25-5464", new Employee("Amy Lee"));
        staff.put("2", new Employee("Harry Hacker"));
        staff.put("3", new Employee("Gary Cooper"));
        staff.put("4", new Employee("Francesca"));

        System.out.println(staff);

        staff.remove("144-25-5464");

        System.out.println(staff);

        staff.put("2", new Employee("ziemniaki"));
        System.out.println(staff);

        System.out.println(staff.get("3"));

        staff.forEach((k,v)-> System.out.println("key=" + k + ", value=" + v));

        Comparator<String> stringComparator = new Comparator<>() {
            @Override
            public int compare(String o1, String o2) {
                return 0;
            }
        };
        TreeMap<String, Integer> myTreeMap = new TreeMap<String, Integer>(Collections.reverseOrder());
        myTreeMap.put("abc", 3);

    }
}
