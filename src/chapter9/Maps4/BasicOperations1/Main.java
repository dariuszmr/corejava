package chapter9.Maps4.BasicOperations1;

import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {

        Map<Integer, String> myMap = new HashMap<>();

        myMap.put(1, "darek");
        myMap.put(2, "monia");
        myMap.put(3, "wenonika");
        myMap.put(4, "jasiu");
        myMap.put(5, "faustynka");
        myMap.put(6, "tereska");
        myMap.put(7, "tadziu");

        System.out.println(myMap);

        var retValue = myMap.put(4, "test");
        System.out.println(retValue);
        System.out.println(myMap);


    }
}
