package chapter9.ConcreteCollections3.LinkedList1;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

public class Main {
    public static void main(String[] args) {
        var staff = new LinkedList<String>();
        staff.add("Amy");
        staff.add("Bob");
        staff.add("Carl");
        System.out.println(staff);
        Iterator<String> iter = staff.iterator();
        String first = iter.next();
        String second = iter.next();
        iter.remove();
        System.out.println(staff);

        staff.add("Bob");
        System.out.println(staff);
        ListIterator<String> iter1 = staff.listIterator();
        ListIterator<String> iter2 = staff.listIterator();
        iter1.next();
        iter1.remove();
        System.out.println(staff);
        iter2.next();
    }
}
