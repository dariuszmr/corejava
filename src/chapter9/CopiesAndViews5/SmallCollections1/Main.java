package chapter9.CopiesAndViews5.SmallCollections1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static java.util.Map.entry;
import static java.util.Map.ofEntries;

public class Main {
    public static void main(String[] args) {
        Map<String, Integer> scores = Map.of("darek", 1, "monia", 2);
        System.out.println(scores);

        Map<String, Integer> anotherScores = ofEntries(
                entry("darek", 1),
                entry("monia", 2)
        );
        System.out.println(anotherScores);

//        scores.remove("darek");
        var names = new ArrayList<>(List.of("darek", "monia"));
        System.out.println(names);
        names.remove(0);
        System.out.println(names);

        var names1 = new ArrayList<>(List.of("darek", "monia", "darek"));
        System.out.println(names1);

        List<String> darek = Collections.nCopies(4, "darek");
        System.out.println(darek);

    }


}
