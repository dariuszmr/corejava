package chapter9.CopiesAndViews5.CheckedViews4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        var strings = new ArrayList<String>();
        ArrayList rawList = strings;
        rawList.add(new Date());

        System.out.println(rawList);

        List<String> safeStrings = Collections.checkedList(strings, String.class);
//        safeStrings.add(new Date());


    }
}
