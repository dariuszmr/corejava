package chapter9.Algorithms6.Converting6;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> staff = List.of("darek", "marek", "zdzichu");
        String[] array = staff.toArray(String[]::new);
        System.out.println(Arrays.toString(array));
    }
}
