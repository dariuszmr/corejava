package chapter9.Algorithms6.Sorting2;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        List<String> myList = Stream.of("darek", "monia", "aaa").sorted(String::compareTo).collect(Collectors.toList());

        System.out.println(myList);
    }
}
