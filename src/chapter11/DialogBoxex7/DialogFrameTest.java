package chapter11.DialogBoxex7;

import javax.swing.*;

public class DialogFrameTest {
    public static void main(String[] args) {
        DialogFrame dialogFrame = new DialogFrame();
        dialogFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        dialogFrame.setVisible(true);
    }
}
