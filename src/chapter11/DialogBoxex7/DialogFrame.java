package chapter11.DialogBoxex7;

import javax.swing.*;

public class DialogFrame extends JFrame {
    private static final int DEFAULT_WIDTH = 300;
    private static final int DEFAULT_HEIGHT = 200;
    private AboutDialog dialog;

    public DialogFrame() {
        setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);

        var menuBar = new JMenuBar();
        setJMenuBar(menuBar);
        var fileMenu = new JMenu("File");
        menuBar.add(fileMenu);

        var aboutItem = new JMenuItem("About");
        aboutItem.addActionListener((event) -> {
            if (dialog == null)
                dialog = new AboutDialog(DialogFrame.this);
            dialog.setVisible(true);
        });
        fileMenu.add(aboutItem);

        var exitItem = new JMenuItem("Exit");
        exitItem.addActionListener((e -> System.exit(0)));
        fileMenu.add(exitItem);
    }

}
