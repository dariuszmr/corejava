package chapter11.DialogBoxex7;

import javax.swing.*;
import java.awt.*;

public class AboutDialog extends JDialog {
    public AboutDialog(JFrame owner) {
        super(owner, "About Dialog Test", true);

        add(new JLabel(
                "<html><h1><i>Core Java</i></h1><hr>By Darek Mrugała</html>"
        ));

        var ok = new JButton("OK");
        ok.addActionListener(event -> setVisible(false));

        var panel = new JPanel();
        panel.add(ok);
        add(panel, BorderLayout.SOUTH);

        pack();
    }
}
