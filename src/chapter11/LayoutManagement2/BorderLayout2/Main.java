package chapter11.LayoutManagement2.BorderLayout2;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Main {

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            JFrame jFrame = new JFrame();
            jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            jFrame.setSize(300,200);
            jFrame.setVisible(true);

            var panel = new JPanel();
            panel.add(new JButton("yellow"));
            panel.add(new JButton("blue"));
            panel.add(new JButton("red"));

            jFrame.add(panel, BorderLayout.SOUTH);
        });
    }

}
