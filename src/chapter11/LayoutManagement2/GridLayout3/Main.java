package chapter11.LayoutManagement2.GridLayout3;

import java.awt.EventQueue;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Main {

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            JFrame jFrame = new JFrame();
            jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            jFrame.setSize(300,200);
            jFrame.setVisible(true);

            JPanel jPanel = new JPanel();
            jPanel.setLayout(new GridLayout(4,4));
            jPanel.add(new JButton("1"));
            jPanel.add(new JButton("2"));
            jPanel.add(new JButton("3"));
            jPanel.add(new JButton("4"));
            jPanel.add(new JButton("5"));
            jPanel.add(new JButton("6"));
            jPanel.add(new JButton("7"));
            jPanel.add(new JButton("8"));
            jPanel.add(new JButton("9"));
            jPanel.add(new JButton("0"));
            jPanel.add(new JButton("."));
            jPanel.add(new JButton("*"));
            jPanel.add(new JButton("+"));
            jPanel.add(new JButton("-"));
            jPanel.add(new JButton("/"));
            jPanel.add(new JButton("="));

            jFrame.add(jPanel);
        });
    }
}
