package chapter11.TextInput3.ScrollPanes5;

import java.awt.EventQueue;
import javax.swing.JFrame;

public class TextComponentFrameTest {

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {


            TextComponentFrame textComponentFrame = new TextComponentFrame();
            textComponentFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            textComponentFrame.setSize(300, 200);
            textComponentFrame.setVisible(true);

        });
    }

}
