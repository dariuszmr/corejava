package chapter11.TextInput3.TextFields1;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Main {

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            JFrame jFrame = new JFrame();
            jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            jFrame.setSize(300, 200);
            jFrame.setVisible(true);

            JPanel jPanel = new JPanel();
            var textField = new JTextField("Default input", 20);
            jPanel.add(textField);

            jFrame.add(jPanel);
        });
    }

}
