package chapter11.ChoiceComponents4.ComboBoxes4;

import javax.swing.*;

public class ComboBoxFrameTest {
    public static void main(String[] args) {
        ComboBoxFrame comboBoxFrame = new ComboBoxFrame();
        comboBoxFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        comboBoxFrame.setSize(600, 300);
        comboBoxFrame.setVisible(true);
    }
}
