package chapter11.ChoiceComponents4.RadioButtons2;

import javax.swing.*;
import java.awt.*;

public class Main {
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            JFrame jFrame = new JFrame();
            jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            jFrame.setSize(300, 200);
            jFrame.setVisible(true);

            var group = new ButtonGroup();

            var smallButton = new JRadioButton("Small", false);
            group.add(smallButton);

            var mediumButton = new JRadioButton("Medium", true);
            group.add(mediumButton);

            JPanel jPanel = new JPanel();
            jPanel.add(smallButton);
            jPanel.add(mediumButton);

            jFrame.add(jPanel);

        });
    }
}
