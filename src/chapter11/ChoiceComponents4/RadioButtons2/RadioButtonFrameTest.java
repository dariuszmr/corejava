package chapter11.ChoiceComponents4.RadioButtons2;

import javax.swing.*;
import java.awt.*;

public class RadioButtonFrameTest {
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            RadioButtonFrame radioButtonFrame = new RadioButtonFrame();
            radioButtonFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            radioButtonFrame.setSize(900, 300);
            radioButtonFrame.setVisible(true);
        });
    }
}
