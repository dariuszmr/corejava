package chapter11.ChoiceComponents4.Checkboxes1;

import javax.swing.*;
import java.awt.*;

public class CheckBoxFrameTest {
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            CheckBoxFrame checkBoxFrame = new CheckBoxFrame();
            checkBoxFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            checkBoxFrame.setSize(300, 200);
            checkBoxFrame.setVisible(true);
        });
    }
}
