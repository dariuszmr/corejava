package chapter11.ChoiceComponents4.Sliders5;

import javax.swing.*;

public class SliderFrameTest {
    public static void main(String[] args) {
        SliderFrame sliderFrame = new SliderFrame();
        sliderFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        sliderFrame.setSize(400, 400);
        sliderFrame.setVisible(true);
    }
}
