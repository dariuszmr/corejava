package chapter11.ChoiceComponents4.Borders3;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

public class Main {
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            JFrame jFrame = new JFrame();
            jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            jFrame.setSize(300,200);
            jFrame.setVisible(true);

            Border etched = BorderFactory.createEtchedBorder();
            Border titled = BorderFactory.createTitledBorder(etched, "A Title");
            JPanel jPanel = new JPanel();
            jPanel.setBorder(titled);
//            jPanel.setBorder(etched);
            jFrame.add(jPanel);
        });
    }
}
