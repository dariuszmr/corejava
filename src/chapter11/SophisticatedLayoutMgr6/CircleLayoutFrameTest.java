package chapter11.SophisticatedLayoutMgr6;

import javax.swing.*;

public class CircleLayoutFrameTest {
    public static void main(String[] args) {
        CircleLayoutFrame circleLayoutFrame = new CircleLayoutFrame();
        circleLayoutFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        circleLayoutFrame.setSize(300, 200);
        circleLayoutFrame.setVisible(true);
    }
}
