package chapter11.SophisticatedLayoutMgr6;

import javax.swing.*;

public class FontFrameTest {
    public static void main(String[] args) {
        FontFrame fontFrame = new FontFrame();
        fontFrame.setSize(300, 200);
        fontFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        fontFrame.setVisible(true);
    }
}
