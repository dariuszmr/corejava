package chapter5;

import java.time.LocalDate;
import java.time.Month;
import java.time.Year;

public class Employee implements Comparable<Employee> {
    final private String name;
    final private double salary;
    final private LocalDate hireDay;

    public Employee(String name, double salary, int year, int month, int day) {
        this.name = name;
        this.salary = salary;
        hireDay = LocalDate.of(year, month, day);
    }

    public Employee(String name, int salary) {
        this(name, salary, LocalDate.now().getYear(), LocalDate.now().getMonthValue(), LocalDate.now().getDayOfMonth());
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", salary=" + salary +
                ", hireDay=" + hireDay +
                '}';
    }

//    @Override
    public int compareTo(Employee o) {
        return Double.compare(this.salary, o.salary);
    }
}
