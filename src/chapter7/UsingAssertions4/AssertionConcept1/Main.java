package chapter7.UsingAssertions4.AssertionConcept1;

public class Main {
    public static void main(String[] args) {
        checking(3);
    }

    public static void checking(int x) {
        assert x >= 0 : x;
    }
}
