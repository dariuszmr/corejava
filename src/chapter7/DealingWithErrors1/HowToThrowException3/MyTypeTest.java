package chapter7.DealingWithErrors1.HowToThrowException3;

public class MyTypeTest {
    public static void main(String[] args) {
        System.out.println("no to próbujemy throw my type");
        try {
            throw new MyType();
        } catch (MyType e) {
            throw new RuntimeException(e);
        }
    }
}
