package chapter7.Logging5.LoggingRecipe8;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LoggingImageViewer {
    public static void main(String[] args) {
        if (System.getProperty("java.util.logging.config.class") == null && System.getProperty("java.util.logging.config.file") == null) {
            try {
                Logger.getLogger("chapter7.Logging5.LoggingRecipe8").setLevel(Level.ALL);
                final int LOG_ROTATION_COUNT = 10;
                var handler = new FileHandler("%h/LoggingImageViewer.log", 0, LOG_ROTATION_COUNT);
                Logger.getLogger("chapter7.Logging5.LoggingRecipe8").addHandler(handler);
            } catch (IOException e) {
                Logger.getLogger("chapter7.Logging5.LoggingRecipe8").log(Level.SEVERE, "Can't create log file handle", e);
            }
        }

        EventQueue.invokeLater( () -> {
            var windowHandler = new WindowHandler();
            windowHandler.setLevel(Level.ALL);
            Logger.getLogger("chapter7.Logging5.LoggingRecipe8").addHandler(windowHandler);

            var frame = new ImageViewerFrame();
            frame.setTitle("LoggingImageViewer");
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            Logger.getLogger("chapter7.Logging5.LoggingRecipe8").fine("Showing frame");
            frame.setVisible(true);
        } );

    }
}
