package chapter7.Logging5.ChangingLogManagerConf3;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    private static final Logger myLogger = Logger.getLogger("chapter7.Logging5.Basic1");

    public static void main(String[] args) {
        Logger.getGlobal().setLevel(Level.WARNING);
        Logger.getGlobal().info("Jasiu lubi komputery");

        System.out.println();

        myLogger.setLevel(Level.WARNING);
        myLogger.info("hej to mój logger");

    }
}
