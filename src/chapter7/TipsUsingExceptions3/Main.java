package chapter7.TipsUsingExceptions3;

import java.util.Objects;

public class Main {

    public static void main(String[] args) {
        checkNotNull(null);
    }

    public static void checkNotNull(Object newValue) {
        Objects.requireNonNull(newValue );
        newValue.toString();
        System.out.println("newValue is not null");
    }

}
