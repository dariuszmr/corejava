package chapter7.DebbugingTips6;

import java.util.logging.Logger;

public class Main {
    public static void main(String[] args) {
        final int x = 4;
        Logger.getGlobal().warning("x=" + x);
        TestThis testThis = new TestThis();
        testThis.doSth();

    }
}
