package chapter7.DebbugingTips6;

import java.util.logging.Logger;

public class TestThis {
    public void doSth() {
        Logger.getGlobal().info("this=" + this);
        Thread.dumpStack();
    }
}
