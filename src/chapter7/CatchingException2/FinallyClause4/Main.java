package chapter7.CatchingException2.FinallyClause4;

public class Main {

    public static void main(String[] args) {
        System.out.println(parseInt("chicken"));
    }

    public static int parseInt(String s) {
        try {
            return Integer.parseInt(s);
        } finally {
            return 0;
        }
    }
}
