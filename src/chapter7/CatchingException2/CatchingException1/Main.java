package chapter7.CatchingException2.CatchingException1;


public class Main {
    public static void main(String[] args) throws Exception {
        MyClass myClass = new MyClass();
        myClass.doSth();

        MyClassChild myClassChild = new MyClassChild();
        myClassChild.doSth();
    }
}
