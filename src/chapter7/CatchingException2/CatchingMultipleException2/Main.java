package chapter7.CatchingException2.CatchingMultipleException2;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        try {
            throw new MyException();
        } catch (MyException e) {
            e.initCause(new IOException());
            System.out.println(e.getMessage());
            System.out.println(e.getClass().getName());
            System.out.println(e.getCause().getClass().getName());
        }
    }

}
