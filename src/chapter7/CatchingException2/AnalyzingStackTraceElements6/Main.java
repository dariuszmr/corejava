package chapter7.CatchingException2.AnalyzingStackTraceElements6;

import java.io.PrintWriter;
import java.io.StringWriter;

public class Main {

    public static void main(String[] args) {
        var t = new Throwable();
        var out = new StringWriter();
        t.printStackTrace(new PrintWriter(out));
        String decsription = out.toString();
        System.out.println(decsription);
    }

}
